import React from 'react';
import { View } from 'react-native';
import ListContainer from "./List/ListContainer";
import {StatusBar} from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={{marginTop: StatusBar.currentHeight, backgroundColor:'black'}}>
        <ListContainer/>
      </View>
    );
  }
}
