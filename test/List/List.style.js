import {StyleSheet} from 'react-native';

const listStyle = StyleSheet.create({
    containerTile: {
        marginBottom: 10,
        marginTop: 10,
        justifyContent: 'space-between',
        borderWidth: 0.5,
        backgroundColor: '#3e4347'
    },
    tileText: {
        fontFamily: 'serif',
        textAlign: 'right',
        color: 'white',
        fontSize: 12,
    },
    tileInfo: {
        padding: 'auto',
        flex: 1,
        justifyContent: 'space-between'
    },
    textClickDetails: {
        fontFamily: 'serif',
        textAlign: 'left',
        color: '#f6c5f9',
        fontSize: 10,
        marginBottom: 1,
    },
    tileTextforDetails: {
        fontFamily: 'serif',
        textAlign: 'center',
        color: '#f6c5f9',
        fontSize: 14,
        padding: 1,
    },
    buttonCloseDetails: {
        height: 10,
        width: 10,
    },
});

export default listStyle