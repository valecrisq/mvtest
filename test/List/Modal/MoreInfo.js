import React from 'react';
import {Button, WebView, Modal, Text, View} from 'react-native';
import listStyle from "../List.style";

export default class MoreInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            details: require('../../Data/details_response'),
        }
    }


    render() {

        let detailsOPH = this.state.details.data.open_hours.map((element, index) => {
            return <View key={index} style={{display: 'flex'}}>
                <Text style={listStyle.tileTextforDetails}>{element.day} - {element.open} - {element.close}</Text>
            </View>

        });


        const modal = <Modal
            animationType="slide"
            transparent={false}
            visible={this.props.isModalVisible}
            onRequestClose={this.props.onRequestClose}>
            <WebView
                source={{uri: 'https://www.openstreetmap.org/export/embed.html?bbox=-0.1573276519775391%2C51.4970418431522%2C-0.10067939758300783%2C51.520226442282826&amp'}}/>
            <View style={{backgroundColor: 'gray'}}>
                <Text style={listStyle.tileTextforDetails}>Title: {this.state.details.data.title}</Text>
                <Text style={listStyle.tileTextforDetails}>Brand: {this.state.details.data.brand}</Text>
                <Text style={listStyle.tileTextforDetails}>Phone: {this.state.details.data.phone}</Text>
                <Text style={listStyle.tileTextforDetails}>Location: {this.state.details.data.location_title}</Text>
                <Text style={{fontWeight: 'bold', color: 'white', textAlign: 'center'}}>Opening Hours</Text>
                {detailsOPH}
                <Button
                    onPress={this.props.onPress}
                    title={'Close Details'}
                    color={'#8e1a00'}
                    borderRadius={100}
                />
            </View>
        </Modal>;


        return (
            <View>
                {modal}
            </View>
        );
    }
}