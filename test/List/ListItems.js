import React from 'react';
import {FlatList, View} from 'react-native';
import Item from './Item'


export default class extends React.Component {

    render() {

        const listItems =
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={[{key: this.props.list.data}]}
                renderItem={(item) =>
                    <Item
                        list={this.props.list}
                        item={item}
                    />
                }
            />;


        return (

            <View>
                {listItems}
            </View>
        );
    }
}
