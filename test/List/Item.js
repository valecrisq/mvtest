import React from 'react';
import {View, Text} from 'react-native';
import {Tile} from 'react-native-elements';
import listStyle from './List.style';
import MoreInfo from './Modal/MoreInfo';


export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
        }
    }

    openModal() {
        this.setState({
            isModalVisible: true,
        });
    }

    closeModal() {
        this.setState({
            isModalVisible: false,
        });
    }


    render() {


        const item = this.props.list.data.map((element, id) => {
            return <View key={id}
                         style={listStyle.containerTile}>
                <Tile
                    onPress={() => this.openModal()}
                    imageSrc={{uri: element.image}}
                    title={element.title}
                    titleStyle={{color: 'white', fontSize: 18, fontFamily: 'serif'}}
                    contentContainerStyle={{height: 100}}
                >

                    <View style={listStyle.tileInfo}>
                        <Text style={listStyle.tileText}>{element.subtitle}</Text>
                        <Text style={listStyle.tileText}>Type: {element.type}</Text>
                        <Text style={listStyle.textClickDetails}>Click for more details!</Text>
                        <View>{element.offers}</View>
                    </View>
                </Tile>
            </View>
        });

        return (
            <View>
                {item}
                {
                    this.state.isModalVisible &&
                    <MoreInfo
                        onPress={() => this.closeModal()}
                        onRequestClose={() => this.closeModal()}
                    />
                }
            </View>
        );
    }
}
