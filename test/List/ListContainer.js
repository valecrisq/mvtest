import React from 'react';
import {View, Text} from 'react-native';
import ListItems from './ListItems';


export default class ListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: require('../Data/list_response'),
        }
    }



    render() {

        return (
            <View>
                <ListItems list={this.state.list}/>
            </View>
        )
    }
}