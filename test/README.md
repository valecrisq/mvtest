# Model Village Coding Test: ReactNative

**I've made this simple App in React Native. I've tryed to made it fluent, with simple styles. Here you can see a list of tiles with an hero image, title and type (campaign or offer). You can check more details clicking on a tile: it will open a modal with a large map (used Open Street View) and other details like location, phone number and opening hours. I requested the help from React Native Elements: this because I needed to import Tiles component.  This dependencies provides to give us an all-in-one UI kit for creating apps in react native.**

## Getting Started

**For start the project you need to install npm package:**

	npm install



Enjoy the app!
